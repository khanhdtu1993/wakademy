module.exports = {
  root: true,
  env: {
    node: true
  },
  'extends': [
    'plugin:vue/essential',
    'eslint:recommended'
  ],
  parserOptions: {
    parser: 'babel-eslint'
  },
  "globals": {
    "_": true,
    "$":true,
    "jQuery": true
  },
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-unreachable': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-unused-vars': 'off', // technicaly for production should be on...
    'vue/require-prop-type-constructor': 'off',
    'no-prototype-builtins': 'off',
    'no-console': 'off',
    // 'no-undef': 'off'
  }
}
