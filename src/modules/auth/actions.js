import router from '@/router'
import {HOME} from '@/router/routes'
import Api from '@/plugins/api'
import AuthUser from '@models/user/authUser'
import i18n from '@/i18n'

export default {

  AUTHENTICATE({commit, dispatch}, {accessToken, preloadedData, redirect = false}) {
    commit('UPDATE_ACCESS_TOKEN', accessToken)
    commit('UPDATE_ACCESS_TOKEN_COOKIE')

    dispatch('INIT', preloadedData)
    commit('SET_LOADED')

    if (redirect) {
      const queryRedirect = router.history.current.query.redirect ?? false

      router.push(
        queryRedirect
          ? { path: queryRedirect }
          : { name: HOME }
      )

    }
  },

  INIT({dispatch, commit, state}, preloadedData = null) {
    console.log('INIT', preloadedData)

    // Use preloaded data if possible, load everything if not
    if (!preloadedData)
      return

    if ('passives' in preloadedData) {
      //console.log('a')
      commit('passives/LOAD_DATA', preloadedData.passives, {root: true})
    }

    if ('workers' in preloadedData)
      commit('SET_WORKERS', preloadedData.workers, {root: true})

    /**
     * Set account data
     */
    if ('account' in preloadedData)
      commit('SET_ACCOUNT', preloadedData.account)

    /**
     * Set user data
     */
    if ('user' in preloadedData) {
      dispatch('SET_USER', preloadedData.user)


      // if ('projects' in preloadedData.user)
      //   dispatch('repositories/projects/SET_ITEMS', preloadedData.user.projects, { root: true })

    }

    if ('admin' in preloadedData && preloadedData.user.is_admin) {
      const admin = preloadedData['admin']

      if ('users' in admin)
        dispatch('repositories/users/SET_ITEMS', admin.users, {root: true})
    }


    // commit('SET_PRELOADED_DATA', {})

    /* Real time updates */
    //Echo.connect()
  },

  SET_USER({commit}, user) {
    commit('SET_USER', AuthUser.create(user))
    if (user.preferences.locale) {
      i18n.locale = user.preferences.locale
    }
  },

  LOGOUT({commit, state}, {redirect = true} = {}) {
    // state.is_logged_in = false
    Api.post('auth/logout')
    commit('CLEAR_ALL_DATA')

    // localStorage.removeItem('state.auth')
    // Echo.disconnect()

    if (redirect) {
      router.push({path: '/login', query: { redirect: router.history.current.path} })
    }
  },

  LOAD({dispatch, commit, state}) {
    commit('SET_PRELOADING', true)
    Api.get('auth/data')
      .then((response) => {
        const accessToken = state.access_token
        const preloadedData = response.preloadedData
        dispatch('AUTHENTICATE', {accessToken, preloadedData})
      })
      .catch(() => {
        dispatch('LOGOUT')
      })
  },

  UPDATE_PREFERENCES({dispatch, commit, state}, preferences) {
    // commit('SET_PRELOADING', true)
    Api.post('auth/preference', preferences)
  },

  RESET_PASSWORD_SUCCESS({dispatch, commit, state}, preferences) {
    router.push({path: '/login'})
  },
}