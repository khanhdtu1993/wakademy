import { mix } from 'mixwith'
import Model from '@abs/model'
import SerializesData from '@abs/concerns/serializes-data'

export default class Document extends mix(Model).with(SerializesData) {
  getTitle() {
    return this.name || 'Undefined'
  }
}
