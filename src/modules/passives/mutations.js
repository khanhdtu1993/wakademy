
export default {
  LOAD_DATA(state, data) {
    Object.keys(data).forEach((key) => {
      state[key] = data[key]
    });
  }
}