import page from './page'
import auth from './auth'
import passives from './passives'
import forms from './forms'
import repositories from './repositories'
//import notification from './notification'

export default {
  auth, forms, repositories, page, passives
}