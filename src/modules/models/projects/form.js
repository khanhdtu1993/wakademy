import { FormState, FormGetters, FormMutations, FormActions } from '@front/modules/abstracts/form'
import Api from '@/plugins/api'
import Store from '@/store'
import Project from './model'
import i18n from '@/i18n'

const state = FormState({
  title: '',
  subtitle: '',
  wp_install: false,
  wp_data: null, // holds response after wp installs
  fields: {
    uuid: null,
    name: null,
    is_locked: false,
    status_id: 1,
    manager_uuid: null,
    designer_uuid: null,
    create_type: 'site',
    accepted_agreement: false,
    accepted_terms: false,
    extra_services: [],
    comments: {},
    fixes: null,
    basic: {
      department_id: null,
      manager_uuid: null,
      brand_name: null,
      description: null,
      business_category_id: null,
    },
    company: {
      email: null,
      phone_number: null,
      address: null,
      working_hours: null,
      selected_hours: [
        { name:'monday', time_from: null, time_till: null },
        { name:'tuesday', time_from: null, time_till: null },
        { name:'wednesday', time_from: null, time_till: null },
        { name:'thursday', time_from: null, time_till: null },
        { name:'friday', time_from: null, time_till: null },
        { name:'saturday', time_from: null, time_till: null },
        { name:'sunday', time_from: null, time_till: null },
      ],
      social_links: [
        { name: 'Facebook', url: null },
        { name: 'Instagram', url: null },
        { name: 'LinkedIn', url: null },
        { name: 'vKontakte', url: null },
      ],
      details_show: false,
      details: [
        { id: 1, name: 'Company name', value: null },
        { id: 2, name: 'Company code', value: null },
        { id: 3, name: 'VAT code', value: null },
        { id: 4, name: 'Bank account number (IBAN)', value: null },
      ]
    },
    website: {
      domain_whmcs: false,
      domain_name: null,
      domain_registered: null,
      domain_care: null,
      domain_com: null,
      care_url: null,
      care_user: null,
      care_password: null,
      host_email: null
    },
    branding: {
      logo: null,
      logo_create: null,
      free_word: null,
      pro_word: null,
      pro_description: null,
      media: []
    },
    style: {
      type: null,
      style: null,
      color_scheme: null,
      colors_selected: [],
      color_perfect_style: null,
      font_head: null,
      font_head_google: null,
      font_head_selected: null,
      font_paragraph: null,
      font_paragraph_google: null,
      font_paragraph_selected: null,
      link1: null,
      link2: null,
      link3: null,
      comments: null,
      link_comments: null
    },
    only_one: { //fields for specified system
      content_all_images: null, // a link to we to download all images of the content.
    },
    sitemap: [],
    sc_title: null, // workaround only for validator to collect "sitemap content title"
    sc_content: null, // workaround only for validator to collect "sitemap content content"
    categories: [],
    payment: {},
    delegate: {},
    conversation: false,
  }
})
const mutations = FormMutations({
  SET_DEPARTMENT(state, value) {
    state.fields.basic.department_id = value
  }
})
const getters = FormGetters({

  VALIDATES: (state) => {
    return {
      basic: {
        department_id: ['required', 'text.please_choose_a_department'],
        brand_name: ['required', 'text.please_enter_your_brand_name'],
        description: ['required', ['text.please_enter_your_description', 'text.please_enter_less_words_under_400_symbols']],
        business_category_id: ['required', 'text.please_select_business_category'],
      },
      company: {
        email: ['required', 'text.please_enter_your_email']
      },
      website: {
        domain_name: ['required', 'text.please_enter_your_domain_name'],
        domain_registered: ['required', 'text.please_select_one_option'],
        domain_care: ['requiredif:domain_registered:provider', 'text.please_select_one_option'],
        care_url: ['requiredif:domain_care:getspace', 'text.please_enter_your_login_url'],
        care_user: ['requiredif:domain_care:getspace', 'text.please_enter_your_login_name'],
        care_password: ['requiredif:domain_care:getspace', 'text.please_enter_your_login_password'],
        host_email: ['required', 'text.please_enter_your_registered_email'],
      },
      branding: {
        logo: ['required', 'text.please_select_one_option'],
        media: ['minIf:logo:owned:1:logo', 'text.please_upload_your_logo'],
        logo_create: ['requiredif:logo:create', 'text.please_select_one_option'],
        free_word: ['requiredif:logo_create:free', 'text.please_enter_word_or_symbol'],
      },
      style: {
        type: ['required', 'text.please_select_one_option'],
        style: ['required', 'text.please_select_one_option'],
        color_scheme: ['required', 'text.please_select_one_option'],
        colors_selected: ['minIf:color_scheme:colors:5', 'text.please_select_at_least_five_colors'],
        font_head: ['required', 'text.please_select_one_option'],
        font_head_selected: ['requiredif:font_head:selected', 'text.please_enter_the_desired_font_url'],
        font_paragraph: ['required', 'text.please_select_one_option'],
        font_paragraph_selected: ['requiredif:font_paragraph:selected', 'text.please_enter_the_desired_font_url'],
        link1: ['url', 'text.please_enter_valid_url_address'],
        link2: ['url', 'text.please_enter_valid_url_address'],
        link3: ['url', 'text.please_enter_valid_url_address'],
      },
      sitemap: ['min:1', 'text.please_add_at_least_one_page'],
      sc_title: ['nestedList:sitemap:required:title', 'text.please_enter_page_title'],
      sc_content: ['nestedList:sitemap:required:content', 'text.please_enter_page_content'],

      //validations for shop
      ...((state.fields.create_type == 'shop') ? {
        categories: ['min:1', 'text.please_add_at_least_one_category'],
      } : {})
    }
  },

})
const actions = FormActions({
  COMPLETE({ state, commit, dispatch }) {
    Api.post('projects/complete', { project_uuid: state.fields.uuid })
    .then((model) => {
      dispatch('repositories/projects/UPDATE_ITEM', model, {root: true})
      commit('UPDATE_STATE', { field: 'show_form', value: false })
      commit('ADD_NOTIFICATION', i18n.t('text.project_completed'), { root: true })
      commit('ADD_NOTIFICATION', i18n.t('text.thank_you_for_using_our_service'), { root: true })
    })
  },
  FINILIZE(context) {
    context.dispatch('VALIDATE_FIELDS').then(() => {
      const model = { ...context.state.fields, finilize: true };

      const success = function() {
        context.commit('UPDATE_STATE', { field: 'show_form', value: false })
        context.commit('ADD_NOTIFICATION', i18n.t('text.submited_successfully'), { root: true })
      };

      (context.getters.IS_EDITING)
        ? context.dispatch('repositories/projects/API_UPDATE', model, {root: true}).then(success)
        : context.dispatch('repositories/projects/API_CREATE', model, {root: true}).then(success)
    })
  },
  SAVE_COMMENTS({ state, commit }) {
    Api.post('projects/save-comments', {
      project_uuid: state.fields.uuid,
      comments: state.fields.comments
    })
    .then(() => {
      commit('ADD_NOTIFICATION', i18n.t('text.comments_have_been_saved'), { root: true })
    })
  },
  SEND_FIXES({ state, commit, dispatch }) {
    Api.post('projects/send-fixes', {
      project_uuid: state.fields.uuid,
      fixes: state.fields.fixes
    })
    .then((model) => {
      commit('UPDATE_STATE', { field: 'show_form', value: false })
      dispatch('repositories/projects/UPDATE_ITEM', model, {root: true})
      commit('ADD_NOTIFICATION', i18n.t('text.fixes_have_been_sent'), { root: true })
    })
  },
  DELEGATE({ state, commit, dispatch }) {
    Api.post('projects/delegate', {
      project_uuid: state.fields.uuid,
      delegate: state.fields.delegate
    })
    .then((responseModel) => {
      //commit('UPDATE_STATE', { field: 'show_form', value: false })
      commit('ADD_NOTIFICATION', i18n.t('text.project_has_been_delegated'), { root: true })
      dispatch('repositories/projects/UPDATE_ITEM', responseModel, { root: true })
    })
  },
  INSTALL_WP({ state, commit, dispatch }) {
    state.wp_install = true
    state.wp_data = null
    Api.get('projects/install/' + state.fields.uuid)
    .then((responseModel) => {
      state.wp_install = false
      state.wp_data = responseModel
      console.log('wp install response: ', responseModel)
    })
  },
  DOWNLOAD_ALL_MEDIA({ state, commit }, files) {
    return Api.download('projects/download-all-media/' + state.fields.uuid, { params: files, responseType: 'arraybuffer' }).then(response => {
      const url = window.URL.createObjectURL(new Blob([response.data, { type: response.headers.get('Content-Type') }]));
      const fileName = response.headers.get('Content-Disposition').split('filename=')[1].split(';')[0];
      const link = document.createElement('a');
      link.href = url;
      link.setAttribute('download', fileName);
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    })
  },

  DOMAIN_CHECK({state}, domain_name) {
    return Api.post('projects/domain/check', {
      domain_name: domain_name,
      project_uuid: state.fields.uuid,
    })
  },

  DOMAIN_REGISTER({state, commit}, domain_name) {
    return Api.post('projects/domain/register', {
      project_uuid: state.fields.uuid,
      domain_name: domain_name
    })
    .then(() => {
      state.fields.website.domain_whmcs = true
      // TODO: update repostiory model..
    })
    .catch((err) => {
      commit('MERGE_ERRORS', err.data.data, 'webiste')
      return err
      //console.log('errors merged', state.validationErrors, err, err.data)
    })
  }
})

/**
 * Exports
 */
export default {
  namespaced: true,

  state,
  getters,
  mutations,
  actions
}
