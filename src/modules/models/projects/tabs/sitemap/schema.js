import { mix } from 'mixwith'
//import { methods as ProductTypeRepository } from '@models/product_types/repository'
//import { methods as ProductMaterialRepository } from '@models/product_materials/repository'

const SitemapSchema = (superclass) => class extends mix(superclass) {
  /**
   * Parse project data that came from API
   */
  static parse(data) {
    const d = super.parse(data)

    d.uuid = data.uuid
    d.page = data.page
    d.title = data.title
    d.subtitle = data.subtitle
    d.content = data.content

    return d
  }

  serialize(options = {}) {
    return {
      ...super.serialize(options),

      uuid = this.uuid,
      page = this.page,
      title = this.title,
      subtitle = this.subtitle,
      content = this.content
    }
  }
}

export default SitemapSchema
