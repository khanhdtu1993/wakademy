import { mix } from 'mixwith'

// import BelongsToCompany from '../concerns/belongs-to-company'
import schema from './schema'
// import ProductFaker from './faker'


class Sitemap extends mix(schema) {
  static create(data) {
    return new this(this.parse(data))
  }
}

export default Sitemap
