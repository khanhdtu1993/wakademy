//import _ from 'lodash'
//import Api from '@/api'
import store from '@/store'
//import { getRepositoryName, getDocumentType } from '@/modules/documents/helpers'
//import Client from '../client'
//import Vendor from '../vendor'
//import Company from '../company'

const UserMethods = (superclass) => class extends superclass {

  isMe() {
    return store.state.auth.user && store.state.auth.user.uuid === this.uuid
  }

  isClient() {
    return this.user_type_id == 1
  }

  isAdmin() {
    return this.user_type_id >= 4
  }

  isSuperAdmin() {
    return this.user_type_id == 5
  }

  isManager() {
    return this.user_type_id == 3
  }

  isDesigner() {
    return this.user_type_id == 2
  }

  isDesignerGTE() { // greater than or euqual
    return this.user_type_id >= 2
  }

  getShortName() {
    const names = this.name.split(' ')
    let inicials = ''
    names.forEach((name, index) => {
      if (index == 0)
        inicials += name + ' '
      else
        inicials += name[0] + '. '
    });
    return inicials
    //return this.name
  }
}

export default UserMethods
