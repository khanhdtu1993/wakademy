import { mix } from "mixwith";
import Model from "@abs/model";
// import UserPreferenceSchema from "./schema";

class UserPreference extends mix(Model) {
  static parse(data) {
    return {
      key: data.key,
      value: data.value,
      user_uuid: data.user_uuid,
    };
  }
}

export default UserPreference;
