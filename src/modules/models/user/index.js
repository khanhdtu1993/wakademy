import { mix } from 'mixwith'
import Model from '@abs/model'
import UserMethods from './methods'

class User extends mix(Model).with(UserMethods) {
  static create(data) {
    return new this(this.parse(data))
  }

  static parse(data) {
    return {
     // ...super.parse(data),

      uuid: data.uuid,
      name: data.username,
      email: data.email,
      is_super: data.is_super,
      is_admin: data.is_admin,
      user_type_id: data.user_type_id,
      department_id: data.department_id,
      project_count: data.project_count
    }
  }
}

export default User
