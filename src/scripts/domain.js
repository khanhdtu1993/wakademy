import store from '@/store'
export default {

  isPL() {
    return store.state.config.department_id == 4
  },

  isLT() {
    return store.state.config.department_id == 3
  },

  isCOM() {
    return store.state.config.department_id == 50
  }
}