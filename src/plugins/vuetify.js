
import Vue from 'vue'
import Vuetify from 'vuetify/lib/framework'

Vue.use(Vuetify)

export default new Vuetify({
  breakpoint: {
    thresholds: {
      sm: 900,
      md: 1200,
    },
    scrollBarWidth: 24,
  },
  theme: {
    themes: {
      light: {
        primary: '#4262ff',
        secondary: '#1c3196',
        accent: '#b64e9a',
        //--
        error: '#e03189',
        info: '#2196F3',
        success: '#4CAF50',
        warning: '#FFC107'
      }
    }
  },
  icons: {
    values: {
      prev: 'ic-chevron-left',
      next: 'ic-chevron-right',
      dropdown: 'ic-expand-more',
      radioOff: 'ic-radio-button-unchecked',
      radioOn: 'ic-radio-button-checked',
      checkboxOff: 'ic-check-box-outline-blank',
      checkboxOn: 'ic-check-box',
      delete: 'ic-close'
    }
  }
})