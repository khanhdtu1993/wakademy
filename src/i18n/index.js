import Vue from 'vue'
import VueI18n from 'vue-i18n'

import en from './en/index'
import lt from './lt/index'
import ru from './ru/index'
import pl from './pl/index'
import bg from './bg/index'
import es from './es/index'
import ro from './ro/index'
import hu from './hu/index'

const messages = {
  en,
  lt,
  ru,
  pl,
  bg,
  es,
  ro,
  hu
}

Vue.use(VueI18n)

// Create VueI18n instance with options
const i18n = new VueI18n({
  locale: 'en', // set locale
  fallbackLocale: 'en',
  messages // set locale messages
})

export const supportedLanguages = ['lt', 'en', 'ru', 'pl', 'bg', 'ro', 'es', 'hu']

export const languagesFlag = [
  {
    name: "English",
    code: "en",
    icon: "UnitedStates.svg",
  },
  {
    name: "Lithuanian",
    code: "lt",
    icon: "Lithuania.svg",
  },
  {
    name: "Russian",
    code: "ru",
    icon: "Russia.svg",
  },
  {
    name: "Polish",
    code: "pl",
    icon: "Poland.svg",
  },
  {
    name: "Bulgarian",
    code: "bg",
    icon: "Bulgaria.svg",
  },
  {
    name: "Spanish",
    code: "es",
    icon: "Spain.svg",
  },
  {
    name: "Romanian",
    code: "ro",
    icon: "Romania.svg",
  },
  {
    name: "Hungary",
    code: "hu",
    icon: "Hungary.svg",
  }
]
export default i18n
