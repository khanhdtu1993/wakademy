import User from '@models/user'

export default {
  SET_PRELOADED_DATA(state, preloadedData) {
    state.preloadedData = preloadedData
  },

  SET_DRAWER(state, value) {
    state.menu_drawer = value
  },

  SET_WORKERS(state, workers) {
    state.workers = workers.map(x => User.create(x))
  },

  ADD_NOTIFICATION(state, message) {
    state.notifications.push(message)
  },

  SET_CONFIG(state, config) {
    state.config = config
  },

  SET_CONFIG_PRELOAD(state, value) {
    state.config_preloading = value
  }
}