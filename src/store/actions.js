import Api from '@/plugins/api'
import i18n from '@/i18n'
export default {
  LOAD_CONFIG({commit}) {
    commit('SET_CONFIG_PRELOAD', true)
    Api.get('auth/config')
      .then((config) => {
        commit('SET_CONFIG', config, {root: true})
        commit('SET_CONFIG_PRELOAD', false)
        if (config.language)
          i18n.locale = config.language

      })
      .catch(() => {
        commit('SET_CONFIG_PRELOAD', false)
      })
  }
}