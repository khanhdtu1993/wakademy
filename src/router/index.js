import Vue from 'vue'
import Router from 'vue-router'
import { routes } from './routes'
import { supportedLanguages } from '@/i18n'


Vue.use(Router)

function makeRoute(route, withParams = true) {
  const params = (route.params && withParams) ? makeParams(route.params) : ''
  const path = ((route.path) ? route.path : route.name) + params
  const rewrite = (route.absolute && route.path) ? route.absolute : false

  let r = {
    name: (withParams) ? route.name : route.name + '-root',
    path: (rewrite) ? path : `/${path}`,
    //beforeEnter: IfGuest
  }

  if (route.refresh) {
    r.beforeEnter = refreshRoute
  }

  if (route.page) {
    const component = route.page
    r['component'] = require(`@/pages/${component}.vue`).default
  }

  if (route.meta) {
    r.meta = route.meta
  }

  return r
}

function makeParams(params) {
  let result = ''
  params.forEach(function(p){
    result += `/:${p}`
  })
  return result
}

function makeRoutes() {
  let result = []
  Object.keys(routes).forEach((r) => {
    const route = routes[r]

    if (route.params && route.paramsRoot) {
      result.push(makeRoute(route, false)) // root path, without params...
      result.push(makeRoute(route)) // with params
    }
    else {
      result.push(makeRoute(route))
    }
  })
  return result
}

const router = new Router({
  mode: 'history',
  routes: makeRoutes()
})

/*
router.beforeEach((to, from, next) => {
  const lang = to.params.lang
  const i18n = router.app.$options.i18n

  if (!supportedLanguages.includes(lang))
    return next({
      path: '/' + i18n.fallbackLocale + '/' + routes.home
    })

  if (i18n.locale !== lang)
    i18n.locale = lang

  return next()
})
*/

//function IfGuest(to, from, next) {
router.beforeEach((to, from, next) => {
  //console.log('router', to, from)
  if (!router.app) {
    next({ path: '/login' })
  }
  const auth = router.app.$options.store.state.auth

  if (to.meta.auth !== false && !auth.is_logged_in) {
    // 'not authorized: to login'
    next({ path: '/login', query: { redirect: to.fullPath } })
  } else if (to.query.redirect && auth.is_logged_in) {
    // 'authorized with redirect'
    next({ path: to.query.redirect })
  }
  // redirecting to home if user still tries to enter login/register/reset pages after logged in.
  else if ( auth.is_logged_in && (to.name == 'login' || to.name == 'register' || to.name == 'reset-password')) {
    next({ path: '/'})
  }
  else {
    next()
  }

})

function refreshRoute(to, from, next) {
  next()
  router.go()
}


export default router