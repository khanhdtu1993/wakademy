// AUTH & LANDING
export const HOME = 'home'

export const LOGIN = 'login'
export const REGISTER = 'register'
export const RESET_PASSWORD = 'reset-password'

// AUTEHNTICATED
export const PROJECTS = 'projects'
export const PROJECT_EDIT = 'projectEdit'
export const PROJECT_CREATE = 'projectCreate'
export const USERS = 'users'
export const USER_EDIT = 'userEdit'
export const USER_CREATE = 'userCreate'
export const PROFILE = 'profile'
/**
 * Possible params
    @param {name} String  - route name
    @param {path} String - route path ( default: '/{$name}' )
    @param {absolute} Boolean - determines if path should be absolute, without leading slash
    @param {page} String - path to vue file ( root is: pages folder )
    @param {params} Array - array of params accepting on router: params=['id','name'] -> ${path}/:id/:name
    @param {paramsRoot} Boolean - allows to access route without parameters a.k.a root route, default = false
    @param {refresh} Boolean - refreshes route, if we need to jump outside vue-router
 */

export const routes = {
  //
  //blog: { name: BLOG,  refresh: true},  page: '_public/blog/Blog', params: ['name'], paramsRoot: true, meta:{new_meta: true}},
  login: { name: LOGIN, meta: { auth: false } },
  register: { name: REGISTER, meta: { auth: false } },
  reset_password: {name: RESET_PASSWORD, meta: { auth: false }},


  //protected routes
  home: { name: HOME,  path: '/', page: 'Home'},
  projects: { name: PROJECTS, page: 'projects/Index' },
  projectCreate : { name: PROJECT_CREATE, path: 'projects/create', page: 'projects/Project'},
  projectEdit : { name: PROJECT_EDIT, path: 'projects/:id', page: 'projects/Project'},
  users: { name: USERS, page: 'users/Index' },
  userCreate : { name: USER_CREATE, path: 'users/create', page: 'users/User'},
  userEdit : { name: USER_EDIT, path: 'users/:id', page: 'users/User'},
  profile: { name: PROFILE, page: 'profile/Index' },
}

export default {
  computed: {
    routes() {
      return routes
    }
  }
}
