import ModMixin from './ModMixin'

export default  {
  mixins: [ ModMixin ],

  data: function() {
    return {
      formName: 'Form',
      formFolderPath: null,
      formFolderPathSuffix: null,
      formEvents: {
        'form-cancel': this.formActionCancel,
        'form-create': this.formActionCreate,
        'form-update': this.formActionUpdate
      },
    }
  },

  computed: {
    // pageTitle() {
    //   return 'Page Title'
    // },
    // Bindable data 4 AbstractDataPage
    // dataBind() {
    //   return {
    //     modName: this.modName,
    //     pageTitle: this.pageTitle,
    //     formComponent: this.formComponent,
    //     formEvents: this.dtFormEvents,
    //   }
    // },

    formComponentPath() {
      let folder = (this.formFolderPath) ? `${this.formFolderPath}/${this.modName}` : this.modName
      if (this.formFolderPathSuffix)
        folder += `/${this.formFolderPathSuffix}`
      return `${folder}/${this.formName}`
    },

    // form component 4 AbstractDataPage
    formComponent() {
      return require(`@/pages/${this.formComponentPath}`).default
    }
  },

  methods: {

    // FORM EVENTS
    formActionCancel() {
      this.$router.push({ name: this.modName, query: this.tableFilter });
    },

    formActionCreate(model) {
      this.repoDispatch('API_CREATE', model)
            .then(this._afterApi)
            .then(() => {
              this.$router.push({ name: `${this.repoGetter('API_RESOURCE_NAME')}Edit`, params: { id: model.uuid } });
              this.$store.commit('ADD_NOTIFICATION', this.$t('text.created_successfully'))
            })
            .catch((e) => {
              this.$store.commit('ADD_NOTIFICATION', this.$t('text.oops_server_error_try_again_later'))
            })
    },

    formActionUpdate(model) {
      this.repoDispatch('API_UPDATE', model)
            .then(this._afterApi)
            .then(() => {
              this.$store.commit('ADD_NOTIFICATION', this.$t('text.updated_successfully'))
            })
            .catch(() => {
              this.$store.commit('ADD_NOTIFICATION', this.$t('text.oops_server_error_try_again_later'))
            })
    },

    _afterApi(responseModel) {
      this.formCommit('CLEAR_FIELDS')
      this.formCommit('CLEAR_ERRORS')
      this.formCommit('SET_FIELDS', responseModel)
      this.formCommit('UPDATE_STATE', { field: 'is_edit', value: true })
      this.formCommit('UPDATE_STATE', { field: 'show_form', value: true })
    }
  },
}
