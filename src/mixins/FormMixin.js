import Store from '@/store'
import ModMixin from './ModMixin'
import PassivesMixin from './PassivesMixin'
import AbstractForm from '@/components/forms/AbstractForm'
import vuetize from '@/scripts/vuetize'
import Button from '@/components/common/buttons/Button'
import { generateFormFields } from '@/components/forms/fieldGenerator'

export default (formName) => {
  return {
    components: { parent: AbstractForm },
    mixins: [ ModMixin, PassivesMixin ],

    data: function() {
      return {
        title: '',
        subtitle: '',
        showFormSubtitle: false,
        modName: formName,
        validateFields: true
      }
    },

    computed: {
      formBaseActions() {
       return {
          spacer: { component: 'v-spacer', order: 50 },
          //cancel: { label: this.$t('text.cancel'), icon: 'ic-close', evinput: this.formBaseActionCancel, component: Button, flat: true, small: true, color:'red', order: 98 },
          save: { label: this.$t('text.save'), icon: 'ic-done', evinput: this.formBaseActionSave, component: Button, flat: true, small: true, color:'primary', order: 99 },
        }
      },
      formRef() {
        return this.$refs.parent.$refs.form
      },

      formBind() {
        return {
          formActions: vuetize(this._formActions),
          ref: 'parent',
          title: this.formState.title ?? '',
          subtitle: this.formState.subtitle ?? '',
          showSubtitle: this.showFormSubtitle
        }
      },

      formActions() {
        return {}
      },

      _formActions() {
        return _.merge(this.formBaseActions, this.formActions)
      },

      errors() {
        return this.formState.validationErrors
      },

      modelData() {
        return this.formState.model
      },

      ...generateFormFields(formName, 'fields')
    },

    methods: {
      formBaseActionCancel() {
        this.$emit('form-cancel')
      },
      formBaseActionSave() {
        if (this.validateFields && this.formRef.validate())
          this.formDispatch('VALIDATE_FIELDS').then(() => {
            this.formEmitData()
          })

        else if(!this.validateFields)
          this.formEmitData()
      },

      formEmitData() {
        const ev = (this.formGetter('IS_EDITING')) ? 'form-update' : 'form-create'
        this.$emit(ev, this.formState.fields)
      }
    }
  }
}

