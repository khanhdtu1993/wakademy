const path = require('path');
const version = require('./package.json').version

module.exports = {
  "publicPath": (process.env.NODE_ENV === 'development') ? "/" : "/static/",
  "transpileDependencies": [
    "vuetify"
  ],

  /*devServer: {
    proxy: {
      '/*': {
        target: "http://wacadem.local",
        logLevel: 'debug'
      }
    }
  },*/

  css: {
    loaderOptions: {
      scss: {
        prependData: `@import "@/styles/variables.scss";`
      }
    },
    extract: {
      filename: `css/[name].[hash].${version}.css`,
      chunkFilename: `css/[name].[hash].${version}.css`
    }
    // sourceMap: true
    // requireModuleExtension: false
  },

  configureWebpack: {
    output: {
      filename: `js/[name].[hash].${version}.js`,
      chunkFilename: `js/[name].[hash].${version}.js`,
    },
    resolve: {
      alias: {
        "@front": path.resolve(__dirname, 'src/'), // legacy use instead @
        "@models": path.resolve(__dirname, 'src/modules/models/'),
        "@abs": path.resolve(__dirname, 'src/modules/abstracts/')
      }
    },

    plugins: []
  },

  // Pug Loader
  chainWebpack: (config) => {
    config.module
      .rule('pug').test(/\.pug$/)
      .use('pug-plain-loader').loader('pug-plain-loader')
      .end();
  },

  pluginOptions: {
    i18n: {
      locale: 'en',
      fallbackLocale: 'en',
      localeDir: 'i18n',
      enableInSFC: true
    },
  },
}